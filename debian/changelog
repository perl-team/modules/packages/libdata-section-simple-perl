libdata-section-simple-perl (0.07-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libdata-section-simple-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 11:28:37 +0100

libdata-section-simple-perl (0.07-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 12 Jun 2022 23:15:10 +0100

libdata-section-simple-perl (0.07-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Sun, 28 Jan 2018 15:18:05 +0200

libdata-section-simple-perl (0.07-1) unstable; urgency=medium

  * New upstream release.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 May 2014 16:58:56 +0200

libdata-section-simple-perl (0.06-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Xavier Guimard ]
  * Imported Upstream version 0.06
  * Update debian/copyright years
  * Bump Standards-Version to 3.9.5

 -- Xavier Guimard <x.guimard@free.fr>  Tue, 22 Apr 2014 06:23:04 +0200

libdata-section-simple-perl (0.05-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * Imported Upstream version 0.05
  * debian/copyright:
    + bump format to 1.0
    + update years
  * Bump Standards-Version to 3.9.4

 -- Xavier Guimard <x.guimard@free.fr>  Tue, 25 Jun 2013 06:21:10 +0200

libdata-section-simple-perl (0.03-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Correct Vcs-Browser and Vcs-Git URLs.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright for inc/Module/*.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 Sep 2011 22:06:19 +0200

libdata-section-simple-perl (0.02-1) unstable; urgency=low

  * Initial Release. (Closes: #637476)

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 11 Aug 2011 22:50:40 +0200
